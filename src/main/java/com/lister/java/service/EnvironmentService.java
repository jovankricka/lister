package com.lister.java.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lister.java.dao.EnviromentDao;
import com.lister.java.model.Environment;

@Service
public class EnvironmentService {

	@Autowired
	EnviromentDao environmentDao;

	@Transactional
	public List<Environment> getAllEnvironments() {
		return environmentDao.getAllEnvironments();
	}

	@Transactional
	public void createEnvironment(Environment environment) {
		environmentDao.createEnvironment(environment);
	}

}
