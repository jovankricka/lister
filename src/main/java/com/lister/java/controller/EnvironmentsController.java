package com.lister.java.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.lister.java.model.Environment;
import com.lister.java.service.EnvironmentService;

@Controller
public class EnvironmentsController {

	public static final String ALL_ENVIRONMENTS = "ALL_ENVIRONMENTS";

	@Autowired
	EnvironmentService environmentService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView getEnvironmentList() {
		ModelAndView modelAndView = new ModelAndView("environments");
		modelAndView.addObject(ALL_ENVIRONMENTS, environmentService.getAllEnvironments());
		return modelAndView;
	}

	@RequestMapping(value = "/about", method = RequestMethod.GET)
	public ModelAndView openAboutPage() {
		ModelAndView modelAndView = new ModelAndView("about");
		return modelAndView;
	}

	@RequestMapping(value = "/addEnvironment", method = RequestMethod.POST)
	public ModelAndView addNewEnvironment(@ModelAttribute("environment") Environment environment) {
		environmentService.createEnvironment(environment);
		ModelAndView modelAndView = new ModelAndView("environments");
		modelAndView.addObject(ALL_ENVIRONMENTS, environmentService.getAllEnvironments());
		return modelAndView;
	}

}
