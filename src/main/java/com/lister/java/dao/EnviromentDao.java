package com.lister.java.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.lister.java.model.Environment;

@Repository
public class EnviromentDao extends AbstractDao {

	@SuppressWarnings("unchecked")
	public List<Environment> getAllEnvironments() {
		Criteria criteria = getSession().createCriteria(Environment.class);
		return criteria.list();
	}

	public void createEnvironment(Environment environment) {
		getSession().save(environment);
	}

}
