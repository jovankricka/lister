<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript"
	src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link
	href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
	rel="stylesheet" type="text/css">
</head>

<body>
	<div class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#navbar-ex-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/lister"><span>Lister</span><br></a>
			</div>
			<div class="collapse navbar-collapse" id="navbar-ex-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li class="active"><a href="/lister">Environments</a></li>
					<li><a href="/lister/about">About</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Server name</th>
								<th>URL</th>
								<th>Status</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${ALL_ENVIRONMENTS}" var="environment"
								varStatus="loop">
								<tr>
									<td>${loop.index + 1}</td>
									<td>${environment.name}</td>
									<td><a href="${environment.url}">${environment.url}</a></td>
									<td><c:out default="None" escapeXml="false"
											value="${environment.status ? \"<i class='fa fa-3x fa-fw fa-check'></i>\" : \"<i class='fa fa-3x fa-fw fa-close'></i>\"}" /></td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<a href="#myModal" role="button" data-toggle="modal"
						class="btn btn-success">Add Environment</a>
				</div>
			</div>
		</div>
	</div>
	<div id="myModal" class="modal fade">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">&times;</button>
					<h4 class="modal-title">Add new server environment</h4>
				</div>
				<form role="form" action="/lister/addEnvironment" method="post">
					<div class="modal-body">
						<div class="section">
							<div class="container">
								<div class="row">
									<div class="col-md-3">

										<div class="form-group">
											<label class="control-label" for="name">Server name</label>
											<input class="form-control" name="name"
												placeholder="name of the server environment" type="text"
												autofocus="autofocus">
										</div>
										<div class="form-group">
											<label class="control-label" for="url">URL</label> <input
												class="form-control" name="url"
												placeholder="location of the server" type="text">
										</div>
										<div class="form-group">
											<label class="control-label" for="status">Status</label> <select
												class="form-control" name="status">
												<option label="Active">true</option>
												<option label="Inactive">false</option>
											</select>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
						<button type="submit" class="btn btn-success">Add</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<img
						src="https://jira.spring.io/secure/attachment/20705/icon_Spring_HighRes.png"
						class="center-block img-responsive">
					<h1>Spring MVC</h1>
					<p>The Spring Web model-view-controller (MVC) framework is
						designed around a DispatcherServlet that dispatches requests to
						handlers, with configurable handler mappings, view resolution,
						locale, time zone and theme resolution as well as support for
						uploading files. The default handler is based on the @Controller
						and @RequestMapping annotations, offering a wide range of flexible
						handling methods. With the introduction of Spring 3.0, the
						@Controller mechanism also allows you to create RESTful Web sites
						and applications, through the @PathVariable annotation and other
						features.</p>
				</div>
			</div>
		</div>
	</div>
	<footer class="section section-info">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<h1>Lister</h1>
					<p contenteditable="true">Servers listing tool.</p>
				</div>
				<div class="col-sm-6">
					<p class="text-info text-right">
						<br> <br>
					</p>
					<div class="row">
						<div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
							<a href="#"><i
								class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 hidden-xs text-right">
							<a href="http://www.facebook.com/jovankricka"><i
								class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</body>

</html>