<!DOCTYPE html>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="text/javascript"
	src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script type="text/javascript"
	src="http://netdna.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
<link
	href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<link
	href="http://pingendo.github.io/pingendo-bootstrap/themes/default/bootstrap.css"
	rel="stylesheet" type="text/css">
</head>

<body>
	<div class="navbar navbar-default navbar-static-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#navbar-ex-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/lister"><span>Lister</span><br></a>
			</div>
			<div class="collapse navbar-collapse" id="navbar-ex-collapse">
				<ul class="nav navbar-nav navbar-right">
					<li><a href="/lister">Environments</a></li>
					<li class="active"><a href="/lister/about">About</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<h1>About Lister</h1>
					<p contenteditable="true">Lister is a simple app for tracking
						your server environments. Lister is implemented with Spring MVC,
						Hibernate and Bootstrap.</p>
					<img
						src="https://jira.spring.io/secure/attachment/20705/icon_Spring_HighRes.png"
						class="center-block img-responsive">
					<h1>Spring MVC</h1>
					<p>The Spring Web model-view-controller (MVC) framework is
						designed around a DispatcherServlet that dispatches requests to
						handlers, with configurable handler mappings, view resolution,
						locale, time zone and theme resolution as well as support for
						uploading files. The default handler is based on the @Controller
						and @RequestMapping annotations, offering a wide range of flexible
						handling methods. With the introduction of Spring 3.0, the
						@Controller mechanism also allows you to create RESTful Web sites
						and applications, through the @PathVariable annotation and other
						features.</p>
				</div>
			</div>
		</div>
	</div>
	<footer class="section section-info">
		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<h1>Lister</h1>
					<p contenteditable="true">Servers listing tool.</p>
				</div>
				<div class="col-sm-6">
					<p class="text-info text-right">
						<br> <br>
					</p>
					<div class="row">
						<div class="col-md-12 hidden-lg hidden-md hidden-sm text-left">
							<a href="#"><i
								class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12 hidden-xs text-right">
							<a href="http://www.facebook.com/jovankricka"><i
								class="fa fa-3x fa-fw fa-facebook text-inverse"></i></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
</body>

</html>